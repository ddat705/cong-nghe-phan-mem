# from os import environ
# from flask_wtf import FlaskForm
from flask import render_template, request, redirect, jsonify
from flask_login import login_user, login_required
from mainapp import app, utils, mail, db, login_manager
from math import ceil
from mainapp.services.auth import authValidate, contactValidate, authValidateAdmin
from mainapp.models import *
from mainapp.utils import *
# from flask_mail import Message
from mainapp.admin_module import *

login_manager.login_view = 'login'
from random import sample


@login_manager.user_loader
def userLoad(userId):
    return BGH.query.get(userId)


@app.route("/")
@app.route("/index")  # @login_required
def index():
    return render_template('dashboard/index.html')


@app.route("/index", methods=['POST'])  # @login_required
def index2():
    return render_template('dashboard/index.html')


@app.route("/tracuu")
def tracuu():
    hs = getlisths()
    return render_template('dashboard/tracuu.html', hs=hs)


# @app.route("/themAccount", methods=['POST', 'GET'])#@login_required
# def themAccount():
#     if request.method == "POST":
#         hs_name = request.form['user']
#         hs_pass = request.form['password']
#         hs_email = request.form['email']
#         hs_first = request.form['firstname']
#         hs_add = request.form['address']
#         hs_last = request.form['lastname']
#         hs_sex = request.form['sex']
#         hs_role = False
#         hs = BGH(username=hs_name, password=hs_pass, email=hs_email, firstname=hs_first, lastname=hs_last,
#                       address=hs_add, role=hs_role, sex=hs_sex)
#         try:
#             db.session.add(hs)
#             db.session.commit()
#             return 'success'
#         except:
#             return 'error'
#     else:
#         return render_template('dashboard/themAccount.html')


# @app.route("/contact", methods=['post', 'get'])
# def contact():
#     if request.method == 'GET':
#         return render_template('dashboard/contact-us.html')
#     if request.method == 'POST':
#         name, email, message = contactValidate(request)
#         msg = Message('Hello',
#                       sender='tienkg5554@gmail.com',
#                       recipients=['tienkg4445@gmail.com'])
#         msg.body = email + "\n" + name + "\n" + message
#         mail.send(msg)
#         return redirect('/')


@app.errorhandler(404)  # @login_required
def notFound(e):
    return render_template('dashboard/404.html'), 404


@app.route('/login')
@app.route('/login.html')
def login():
    return render_template('dashboard/login.html')


@app.route('/login', methods=['POST'])
def login_post():
    user1 = request.form
    user, error, remember = authValidate(request)
    if not user:
        return render_template('dashboard/login.html', error=error)
    login_user(user=user, remember=remember)
    return redirect('index')


@app.route('/signup')  # @login_required
def signup():
    return render_template('dashboard/register.html')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/xemdiem')
def xemdiem():
    return render_template('dashboard/login.html')


@app.route('/login-admin', methods=['POST', 'GET'])
def login_admin():
    if request.method == 'GET':
        return redirect('/BGH')
    if request.method == 'POST':
        user, error = authValidateAdmin(request)
        if not user:
            return "error"
    login_user(user=user)
    return redirect('/admin')


@app.route('/chartareadata')
def chartareadata():
    thisdict = {
        "lop": {
            "hk1": ["9", "9", "9", "9", "9", "9", "9", "9", "9"],
            "hk2": ["9", "9", "9", "9", "9", "9", "9", "9", "9"]
        }

    }
    thisdict = {
        "2020": {
            "hk1": ["9"],
            "hk2": ["9"]
        }

    }

    return jsonify(results=thisdict)


@app.route('/charts')
def charts():
    return render_template('dashboard/charts.html')


DEFAULT_PORT = 1234
if __name__ == "__main__":
    from mainapp.admin_module import *

    app.run(debug=True, port=DEFAULT_PORT)
