import hashlib
from mainapp.models import *


def authValidate(request):  # xacminhtaikhoan
    remember = True if request.form.get('rememberme') else False
    email = request.form.get('email')
    isEmailMatched = BGH.query.filter(BGH.email == email.strip()).first()
    password = request.form.get('password')
    # password = str(hashlib.md5(password.strip().encode('utf-8')).hexdigest())

    isPasswordMatched = BGH.query.filter(BGH.password == password and BGH.email == email).first()

    if not isPasswordMatched or not isEmailMatched:
        return None, "Login error", remember

    user = BGH.query.filter(BGH.email == email.strip(),
                                BGH.password == password).first()
    return user, "True", remember


def authValidateAdmin(request):  # xacminhtaikhoan_admin

    email = request.form.get('email')

    isEmailMatched = BGH.query.filter(BGH.email == email).first()

    password = request.form.get('password')
    # password = str(hashlib.md5(password.strip().encode('utf-8')).hexdigest())

    isPasswordMatched = BGH.query.filter(BGH.password == password and BGH.email == email).first()

    if not isPasswordMatched or not isEmailMatched:
        return None, "Login error"

    user = BGH.query.filter(BGH.email == email.strip(),
                                BGH.password == password).first()
    return user, "True"


def contactValidate(request):
    name = request.form.get('name')
    email = request.form.get('email')
    message = request.form.get('message')
    return name, email, message
