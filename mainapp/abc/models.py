from flask_login import UserMixin
from sqlalchemy import Column, Integer, Float, String, Date, Boolean, ForeignKey, CheckConstraint
from sqlalchemy import Enum
from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy
from mainapp import Admin, db
from datetime import datetime
from flask_admin.contrib.sqla import ModelView
from flask_admin import BaseView, expose
from flask_login import logout_user, current_user
from werkzeug.utils import redirect
from flask import url_for, request



student_subject_table = db.Table(
    "student_subject", db.Model.metadata,
    db.Column("student_id", Integer, db.ForeignKey(
        "Student.id")),
    db.Column("subject_id", Integer, db.ForeignKey(
        "subject.id"))
)

grade_class_table = db.Table(
    "grade_subject", db.Model.metadata,
    db.Column("grade_id", Integer, db.ForeignKey(
        "grade.id")),
    db.Column("subject_id", Integer, db.ForeignKey(
        "subject.id"))
)


grade_teacher_class_table = db.Table(
    "grade_teacher_class", db.Model.metadata,
    db.Column("grade_id", Integer, db.ForeignKey(
        "grade.id")),
    db.Column("teacher_id", Integer, db.ForeignKey(
        "Teacher.id")),
    db.Column("class_id", Integer, db.ForeignKey(
        "Class.id"))
)


class Student(db.Model, UserMixin):
    __tablename__ = 'Student'
    email = Column(String(50), nullable=False, unique=True)
    password = Column(String(100), nullable=False)
    firstname = Column(String(50), nullable=False)
    lastname = Column(String(50), nullable=False)
    address = Column(String(100), nullable=True)
    account_type = Column(String(50))
    age = Column(Integer)
    sex = Column(Enum('Male', 'Female', 'Other'))
    active = Column(Boolean, default=True)  # 1: online, 0:offine

    birthday = Column(Date)
    role = Column(Integer, default=3)  # 1: BGH, 2: Teacher, 3:Student
    id = Column(Integer, unique=True, primary_key=True, autoincrement=True)
    classid = Column(Integer, ForeignKey("Class.id"))
    minors = db.relationship("Subject", secondary=student_subject_table,
                             back_populates="minor_students")
    Payment = Column(Enum('1', '2', '3'))  # 1=Dathanhtoan,2=Dathanhtoan1phan,3=Chuathanhtoan
    # __table_args__ = {'extend_existing': True}


class Teacher(db.Model,UserMixin):
    __tablename__ = 'Teacher'
    # __table_args__ = {'extend_existing': True}
    email = Column(String(50), nullable=False, unique=True)
    password = Column(String(100), nullable=False)
    firstname = Column(String(50), nullable=False)
    lastname = Column(String(50), nullable=False)
    address = Column(String(100), nullable=True)
    account_type = Column(String(50))
    age = Column(Integer)
    sex = Column(Enum('Male', 'Female', 'Other'))
    active = Column(Boolean, default=True)  # 1: online, 0:offine

    birthday = Column(Date)
    role = Column(Integer, default=3)  # 1: BGH, 2: Teacher, 3:Student
    id = Column(Integer, primary_key=True, autoincrement=True)
    subjectid = Column(Integer, ForeignKey("subject.id"))


class BGH(db.Model, UserMixin):
    __tablename__ = 'bgh'
    # __table_args__ = {'extend_existing': True}
    id = Column(Integer,primary_key=True,autoincrement=True)
    email = Column(String(50), nullable=False, unique=True)
    password = Column(String(100), nullable=False)
    firstname = Column(String(50), nullable=False)
    lastname = Column(String(50), nullable=False)
    address = Column(String(100), nullable=True)
    account_type = Column(String(50))
    age = Column(Integer)
    sex = Column(Enum('Male', 'Female', 'Other'))
    active = Column(Boolean, default=True)  # 1: online, 0:offine

    birthday = Column(Date)
    role = Column(Integer, default=3)  # 1: BGH, 2: Teacher, 3:Student


class Grade(db.Model):
    __tablename__ = 'grade'
    # __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    name = Column(String(50))


class Class(db.Model):
    __tablename__ = 'Class'
    # __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Integer, nullable=False, unique=True)
    quantity = Column(Integer, CheckConstraint('quantity >20 and quantity <=40'))
    grade = Column(Integer, ForeignKey("grade.id"))

    def __str__(self):
        return self.name


class Semester(db.Model):
    __tablename__ = 'semester'
    # __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Integer, nullable=False, unique=True)
    schoolyear = Column(Date)
    from_date = Column(Date)
    to_date = Column(Date)

    def __str__(self):
        return self.name


class Score(db.Model):
    __tablename__ = 'Score'
    # __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True, autoincrement=True)
    semester = Column(Integer, ForeignKey("semester.id"))
    student = Column(Integer, ForeignKey("Student.id"))
    scoredetail = relationship("ScoreDetail", backref='Score', lazy=True)


class Subject(db.Model):
    __tablename__ = 'subject'
    # __table_args__ = {'extend_existing': True}
    id = Column(Integer, unique=True, primary_key=True, autoincrement=True)
    name = Column(Integer, nullable=False, unique=False)
    sessions = Column(Integer)
    grade = Column(Integer, ForeignKey("grade.id"))
    price = Column(Integer, nullable=False)
    subjectid = Column(Integer, ForeignKey("subject.id"))
    minor_students = db.relationship("Student",
                                     secondary=student_subject_table,
                                     back_populates="minors")

    def __str__(self):
        return self.name


class ScoreDetail(db.Model):
    __tablename__ = 'scoredetail'
    # __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Integer, nullable=False, unique=False)
    value = Column(Float, nullable=True)
    score = Column(Integer, ForeignKey("Score.id"))


    def __str__(self):
        return self.name


class Phone(db.Model):
    __tablename__ = 'Phone'
    # __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(Integer)
    name = Column(Integer, nullable=False, unique=False)
    value = Column(String(20), nullable=True)
    useremail = Column(Integer, ForeignKey(Student.id), nullable=False)


class Order(db.Model):
    __tablename__ = 'Order'
    id = Column(Integer, primary_key=True, autoincrement=True)
    studentid = Column(Integer, ForeignKey(Student.id))
    ngaybatdau = Column(Date)
    ngayketthuc = Column(Date)
    status = Column(Enum('1', '2', '3'))  # 1=Dathanhtoan,2=Dathanhtoan1phan,3=Chuathanhtoan


class OrderDetail(db.Model):
    __tablename__ = 'OrderDetail'
    id = Column(Integer, primary_key=True, autoincrement=True)
    Orderid = Column(Integer, ForeignKey(Order.id))
    Subject_id = Column(Integer, ForeignKey(Subject.id))
    Subject_price = Column(Integer)
    ngaybatdau = Column(Date)
    ngayketthuc = Column(Date)
    status = Column(Enum('1', '2', '3'))  # 1=Dathanhtoan,2=Dathanhtoan1phan,3=Chuathanhtoan


if __name__ == "__main__":
    db.create_all()
