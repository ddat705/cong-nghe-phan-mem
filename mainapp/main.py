# from os import environ
# from flask_wtf import FlaskForm
import json
import smtplib
import paypalrestsdk
from paypalrestsdk import configure

from mainapp.utils import *
from mainapp.admin_module import *
from flask_login import login_user, login_required
from mainapp import app, utils, mail, db, login_manager
from math import ceil
from mainapp.services.auth import authValidate, contactValidate, authValidateAdmin
from mainapp.models import *
from functools import wraps
# from flask_mail import Message
from mainapp.admin_module import *
from flask import render_template, request, redirect, jsonify, session, current_app, flash

login_manager.login_view = 'login'
from random import sample

paypalrestsdk.configure({
    "mode": "sandbox",  # sandbox or live
    "client_id": "AdpFE5uzDjkw12pjMNkYIUxvlr1sVYntW2o5jrqxABbHxW-7tcUvl7Otd0KVOEsL_RV8sfDgQlqPn1_z",
    "client_secret": "EJ3XBSzYJH7Zs28pQGkHv_PlqJhxC2xZ_c2hbGgAk09vPV0m7luYrbz7nMCnDIVawKY3I5tADjeb5kAG"})


@login_manager.user_loader
def userLoad(userId):
    if (session['type'] == 1):
        return Student.query.get(userId)
    if (session['type'] == 2):
        return Teacher.query.get(userId)
    if (session['type'] == 3):
        return BGH.query.get(userId)


@app.route("/index")
@app.route("/")
def index():
    session['idclassselected'] = 0
    session['defaultsemester'] = 1
    session['defaultgradee'] = 1
    session['idclass'] = 1
    print(session)
    if ('type' in session):
        if session['type'] == 1:
            return render_template('dashboard/Student/index.html', type=session['type'])
        if session['type'] == 2:
            return render_template('dashboard/Teacher/index.html', type=session['type'])
    else:
        session['type'] = 0
        return render_template('dashboard/index.html', type=session['type'])


@app.route("/sendmail", methods=['POST'])
@login_required
def sendmail():
    rs = getlisthsunpayment()
    mes = request.form['msg']
    sever = smtplib.SMTP("smtp.gmail.com", 587)
    sever.starttls()
    sever.login("sdkansdkan1234@gmail.com", "mkmkmk12")
    for item in rs:
        sever.sendmail("sdkansdkan1234@gmail.com", item.email, mes)
    return redirect('admin/sendmail/')


@app.route("/chonlop", methods=['POST'])  # @login_required
@login_required
def index2():
    return render_template('dashboard/index.html')

@app.route("/forgotpassword", methods=['POST'])  # @login_required
def forgotpassword():
    email = request.form['email']
    util_supdate_pass_student_forgot(email)
    return render_template('dashboard/login.html',send="1")


@app.route("/editscore", methods=['POST', 'GET'])
@login_required
def editscore():
    if session['type'] != 2:
        return render_template('dashboard/index.html', type=session['type'])
    if request.method == "GET":
        listscore_and_hs, classes, listtype = load_hs(session['idclassselected'], session['_user_id'])
        print('here')
        print(session)
        return render_template('dashboard/Teacher/editscore.html', listscore_and_hs=listscore_and_hs, classes=classes,
                               listtype=listtype, type=session['type'])
    elif request.method == "POST":
        classid = request.form['idSelected']

        listscore_and_hs, classes, listtype = load_hs(classid, session['_user_id'])
        return render_template('dashboard/Teacher/editscore.html', listscore_and_hs=listscore_and_hs, classes=classes,
                               listtype=listtype, type=session['type'])
    else:
        pass


@app.route("/addstudentclass", methods=['POST'])
@login_required
def addstudentclass():
    idhs = request.form['hiddenid']
    classid = request.form['selectclass']
    addstudentintoclass(idhs, classid)
    return redirect('admin/editstudent/')


@app.route("/addcolumn", methods=['POST'])
@login_required
def addcolumn():
    if session['type'] != 2:
        return render_template('dashboard/index.html', type=session['type'])
    Name = request.form['listype']
    session['idclassselected'] = request.form['name']
    listscore_and_hs, classes, listtype = load_hs(session['idclassselected'], session['_user_id'])
    addcolumnscorestudents(Name, listscore_and_hs)
    return redirect('editscore')


@app.route("/scorechart", methods=['POST', 'GET'])
@login_required
def scorechart():
    if session['type'] != 2:
        return render_template('dashboard/index.html', type=session['type'])
    if request.method == "GET":
        print(session)
        classes = load_listclass(session['_user_id'])
        return render_template('dashboard/Teacher/scorechart.html', classes=classes, type=session['type'])
    elif request.method == "POST":
        classes = load_listclass(session['_user_id'])
        return render_template('dashboard/Teacher/scorechart.html', classes=classes, type=session['type'])
    else:
        pass


@app.route("/hsscore", methods=['POST', 'GET'])
@login_required
def hsscore():
    if session['type'] != 1:
        return render_template('dashboard/index.html', type=session['type'])
    if request.method == "GET":

        ListStudentScoreSubject = load_score_hs(session['_user_id'])
        return render_template('dashboard/hsscore.html', ListStudentScoreSubject=ListStudentScoreSubject,
                               type=session['type'])
    elif request.method == "POST":
        classid = request.form['idSelected']
        ListStudentScoreSubject = load_score_hs()
        return render_template('dashboard/hsscore.html', ListStudentScoreSubject=ListStudentScoreSubject,
                               type=session['type'])
    else:
        pass


@app.route("/studentinformation", methods=['POST', 'GET'])
@login_required
def studentinformation():
    if session['type'] != 1:
        return render_template('dashboard/index.html', type=session['type'])
    if request.method == "GET":
        datainfor, classnamestudent = load_studentinformation(1)
        return render_template('dashboard/studentinformation.html', datainfor=datainfor,
                               classnamestudent=classnamestudent, type=session['type'])
    if request.method == "POST":
        datainfor, classnamestudent = load_studentinformation(1)
        return render_template('dashboard/studentinformation.html', datainfor=datainfor,
                               classnamestudent=classnamestudent, type=session['type'])
    else:
        pass


@app.route("/updatestudentinformation", methods=['POST'])
@login_required
def updatestudentinformation():
    if session['type'] != 1:
        return render_template('dashboard/index.html', type=session['type'])
    idhs = request.form['hiddenid']
    address = request.form['address']
    phone = request.form['phone']
    utils_update_information_student(idhs, address, phone)
    datainfor, classnamestudent = load_studentinformation(idhs)
    return render_template('dashboard/studentinformation.html', datainfor=datainfor, classnamestudent=classnamestudent)


@app.route("/updatestudenpassword", methods=['POST'])
@login_required
def updatestudenpassword():
    if session['type'] != 1:
        return render_template('dashboard/index.html', type=session['type'])
    idhs = request.form['hiddenid']
    oldpass = request.form['passwordold']
    newpass = request.form['passwordnew']
    rs = util_supdate_pass_student(idhs, oldpass, newpass)
    if rs is None:
        datainfor, classnamestudent = load_studentinformation(idhs)
        return render_template('dashboard/studentinformation.html', datainfor=datainfor,
                               classnamestudent=classnamestudent,error="1")
    datainfor, classnamestudent = load_studentinformation(idhs)
    return render_template('dashboard/studentinformation.html', datainfor=datainfor, classnamestudent=classnamestudent,success="1")


@app.route("/thanhtoan")
@login_required
def thanhtoan():
    if session['type'] != 1:
        return render_template('dashboard/index.html', type=session['type'])
    idhs = session['_user_id']
    hs = geths(idhs)

    print(hs.Payment)
    return render_template('dashboard/Student/thanhtoan.html', hs=hs, type=session['type'])


@app.route('/payment', methods=['POST'])
@login_required
def payment():
    paypalrestsdk.configure({
        "mode": "sandbox",  # sandbox or live
        "client_id": "AdpFE5uzDjkw12pjMNkYIUxvlr1sVYntW2o5jrqxABbHxW-7tcUvl7Otd0KVOEsL_RV8sfDgQlqPn1_z",
        "client_secret": "EJ3XBSzYJH7Zs28pQGkHv_PlqJhxC2xZ_c2hbGgAk09vPV0m7luYrbz7nMCnDIVawKY3I5tADjeb5kAG"})

    payment = paypalrestsdk.Payment({
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"},
        "redirect_urls": {
            "return_url": "http://127.0.0.1:5000/thanhtoan",
            "cancel_url": "http://127.0.0.1:5000/thanhtoan"},
        "transactions": [{
            "item_list": {
                "items": [{
                    "name": "Payment Fee",
                    "sku": "12345",
                    "price": "1.00",
                    "currency": "USD",
                    "quantity": 1}]},
            "amount": {
                "total": "1.00",
                "currency": "USD"},
            "description": "This is the payment transaction description."}]})

    if payment.create():
        print('Payment success!')
    else:
        print(payment.error)

    return jsonify({'paymentID': payment.id})


@app.route('/execute', methods=['POST'])
@login_required
def execute():
    success = False

    payment = paypalrestsdk.Payment.find(request.form['paymentID'])

    if payment.execute({'payer_id': request.form['payerID']}):
        flash('Execute success!')
        success = True
        idhs = session['_user_id']
        paymentt(idhs)
    else:
        print(payment.error)

    return redirect('thanhtoan')


@app.route('/test')
@login_required
def test():
    return render_template('dashboard/test.html')


@app.errorhandler(404)  # @login_required
@login_required
def notFound(e):
    return render_template('dashboard/404.html'), 404


@app.route('/login')
@app.route('/login.html')
def login():
    session['type'] = 0
    error = 'none'
    return render_template('dashboard/login.html')


@app.route('/login', methods=['POST'])
def login_post():
    user1 = request.form
    user, error, type = authValidate(request)
    if not user:
        return render_template('dashboard/login.html', error=error)
    session['type'] = type
    login_user(user=user)
    return redirect('index')


@app.route('/signup')  # @login_required
def signup():
    return render_template('dashboard/register.html')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/login-admin', methods=['POST', 'GET'])
def login_admin():
    session['type'] = 3
    session['idclassselected'] = 0
    session['defaultsemester'] = 1
    session['defaultgradee'] = 1
    if request.method == 'GET':
        return redirect('/admin/')
    if request.method == 'POST':
        user, error = authValidateAdmin(request)
        if not user:
            return "error"

    login_user(user=user)
    return redirect('/admin/')


@app.route('/admin/editstudent')
@login_required
def returndata():
    render_template('Admin/index.html', data=2)


@app.route('/api/data_id_score', methods=['POST'])
@login_required
def data_id_score():
    dataidscrore = json.loads(request.data)
    print(dataidscrore)
    update_all_scoredetail(dataidscrore)
    return jsonify(dataidscrore)


@app.route('/data', methods=['POST', 'GET'])
@login_required
def dataSubjectAndScore():
    if request.method == "GET":
        data = load_data_chart_class(session['idclass'])
        return jsonify(data)
    elif request.method == "POST":
        rqdata = json.loads(request.data)
        session['idclass'] = int(rqdata['idclass'])
        return "sth"
    else:
        pass


@app.route('/data2', methods=['POST', 'GET'])
@login_required
def dataSemesterGradeSubject():
    if request.method == "GET":
        data = load_data_chart_semester(session['defaultgradee'], session['defaultsemester'])  # fixxxx
        return jsonify(data)
    elif request.method == "POST":
        rqdata = json.loads(request.data)

        session['defaultsemester'] = int(rqdata['semester'])
        session['defaultgradee'] = int(rqdata['gradeid'])
        return "sth"
    else:
        pass


@app.route('/semester')
def dataSemesterGradeSubjectt():
    data = load_semester()

    return jsonify(data)


@app.route('/grade')
def dataGrade():
    data = load_grade()
    return jsonify(data)


if __name__ == "__main__":
    from mainapp.admin_module import *

    app.run(debug=True)
